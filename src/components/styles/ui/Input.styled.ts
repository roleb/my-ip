import styled from "styled-components";

export const Input = styled.input`
  flex: 0.3 0 200px;
  padding: 10px 20px;
  border-radius: 10px 0 0 10px;
  border: 1px solid silver;
  font-size: 1rem;
`;
