import { YMaps, Map, Placemark } from '@pbe/react-yandex-maps';

function MapWrapper({location}) {
  return (
    <div style={{position: 'absolute', bottom: 0}}>
      <YMaps>
        <div>
          <Map state={{ center: location, zoom: 13 }}>
            <Placemark geometry={location}/>
          </Map>
        </div>
      </YMaps>
    </div>
  )
}
export default MapWrapper