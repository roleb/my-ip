import Search from "./ui/Search"
import {Container} from '../components/styles/Container.styled'
function Header ({onSearchIP}) {
  return (
    <Container>
      <Search onSearchIP={onSearchIP}/>
    </Container>
  )
}
export default Header