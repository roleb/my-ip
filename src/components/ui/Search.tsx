import { Input } from "../styles/ui/Input.styled"
import styled from "styled-components";
import { useState} from 'react'

const Button = styled.button`
  background: #000;
  color: #fff;
  border: 0;
  font-size: 18px;
  cursor: pointer;
  width: 40px;
  border-radius: 0 10px 10px 0;
`
function Search ({onSearchIP}) {
  const [ text, setText ] = useState('')
  function handleChange (e) {
    setText(e.target.value)
  }
  function handClick () {
    onSearchIP(text)
  }
  return (
    <>
    <Input type="text" onChange={handleChange}/>
    <Button onClick={handClick}>&gt;</Button>
    </>
  )
}
export default Search