import styled from "styled-components";
import {Container} from '../components/styles/Container.styled'

const IP = styled.div`
  font-size: clamp(1rem, 3.2vw, 4rem); 
  text-align: center;
  padding: 20px;
  `
  
function UserDetail ({data}) {

const detailBlock = data.city ?
    (<Container style={{gap: '10px'}}>
    Город: <span>{data.city}</span>
    Страна: <span> {data.country}</span>
    Провайдер: <span> {data.isp}</span>
  </Container>):null
  
  return (
    <div>
      <IP>IP: {data.query}</IP>
       {detailBlock}
    </div>
  )
}
export default UserDetail