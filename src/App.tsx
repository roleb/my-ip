import { useEffect, useState } from 'react'
import Header from './components/Header';
import UserDetail from './components/UserDetail';
import MapWrapper from './components/MapWrapper';
import ky from 'ky';
import './App.css'

interface IDetail {
  query: number,
  lat: number,
  lon: number
}
function App() {
  const [userDetail, setUserDetail] = useState({})
  const [location, setLocation] = useState<number[]>([])
  useEffect(() => {
    getUserDetail()
  }, [])
  async function fetchIP (text=''): Promise<IDetail> {
    return await ky.get(`${import.meta.env.VITE_IP_SERVICE}/${text}`).json()
  }
  async function getUserDetail () {
    let detail:IDetail = await fetchIP()
    detail = {query: detail.query, lat: detail.lat, lon: detail.lon}
    setUserDetail(() => detail)
    setLocation(() => [detail.lat, detail.lon])
  }
  async function searchIP (text:string) {
    if (text) {
      const detail = await fetchIP(text)
      setUserDetail(() => detail)
      setLocation(() => [detail.lat, detail.lon])
    }
  }
  return (
    <div className='app'>
      <Header onSearchIP={searchIP}/>
      <UserDetail data={userDetail}/>
      <MapWrapper location={location} />
    </div>
  )
}

export default App
